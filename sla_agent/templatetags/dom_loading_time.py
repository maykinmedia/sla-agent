from django import template


register = template.Library()


@register.inclusion_tag('dom_load_send.html')
def send_dom_loading_time_js():
    return {}

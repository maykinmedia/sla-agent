from django.apps import AppConfig


class SlaAgentConfig(AppConfig):
    name = 'sla_agent'

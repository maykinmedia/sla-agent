import warnings

from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


class SLAAgent(object):

    def __init__(self):
        self.kpis = []
        self.middleware_kpis = []

    def register(self, kpi_class, for_middleware=False):
        kpi = kpi_class()

        if for_middleware or getattr(kpi, 'for_middleware', False):
            self.middleware_kpis.append(kpi)

        self.kpis.append(kpi)

    def check(self):
        names_encountered = set()

        if not hasattr(settings, 'SLA_SITE_TOKEN') or not settings.SLA_SITE_TOKEN:
            warnings.warn("SLA_SITE_TOKEN setting not set")

        for kpi in self.kpis:
            if not kpi.name:
                raise ImproperlyConfigured("KPI {} does not have a name".format(kpi))

            if kpi.name in names_encountered:
                raise ImproperlyConfigured("KPI name {} was used more than once.".format(kpi.name))

            kpi.check()

            names_encountered.add(kpi.name)

        for middleware_kpi in self.middleware_kpis:
            if (not hasattr(middleware_kpi, 'process')
                    or not callable(middleware_kpi.process)):

                raise ImproperlyConfigured(
                    "KPI {name} was registered for middleware but does not have a process "
                    "method".format(name=middleware_kpi.name))

            if not (hasattr(middleware_kpi, 'should_process')
                    or not callable(middleware_kpi.should_process)):

                raise ImproperlyConfigured(
                    "KPI {name} was registered for middleware but does not have a should_process "
                    "method".format(name=middleware_kpi.name))

    def _initialize_kpis(self):
        for kpi in self.kpis:
            kpi.initialize()

    def get_urls(self):
        self.check()
        self._initialize_kpis()

        urlpatterns = []

        for kpi in self.kpis:
            urlpatterns += kpi.get_urls()

        return urlpatterns

    def get_kpi(self, name):
        for kpi in self.kpis:
            if kpi.name == name:
                return kpi

        raise ValueError('No kpi with name {} found.', name)

    @property
    def urls(self):
        return self.get_urls(), 'sla', 'sla'

    def process(self, fq_url_name, request, response):
        for kpi in self.middleware_kpis:
            if kpi.should_process(fq_url_name, request):
                kpi.process(request, response)


sla_agent = SLAAgent()

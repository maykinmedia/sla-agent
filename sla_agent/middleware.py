from django.urls import resolve

from sla_agent.agent import sla_agent


class SLAMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        current_url = resolve(request.path_info)

        if current_url.namespace:
            fq_url_name = '{namespace}:{name}'.format(namespace=current_url.namespace,
                                                      name=current_url.url_name)
        else:
            fq_url_name = current_url.url_name

        response = self.get_response(request)

        sla_agent.process(fq_url_name, request, response)

        return response

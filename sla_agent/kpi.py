import json
import logging

from django.conf import settings
from django.db.models import F
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.utils import timezone

try:
    from django.conf.urls import url as path
except ImportError:
    from django.urls import path

logger = logging.getLogger(__name__)


class KPI(object):
    slug = None
    name = None
    from_middleware = False

    def get_counts(self, site_identifier=None):
        raise NotImplementedError

    def initialize(self):
        pass

    def check(self):
        pass

    @property
    def path(self):
        return self.slug or self.name

    def get_view(self):

        def kpi_view(request):
            token = request.GET.get('token')

            if not token or token != settings.SLA_SITE_TOKEN:
                return HttpResponseForbidden()

            site_identifier = request.GET.get('site')

            value, total = self.get_counts(site_identifier=site_identifier)

            return JsonResponse({'value': value,
                                 'total': total})

        kpi_view.__name__ = self.name

        return kpi_view

    def get_urls(self):
        return [
            path(self.path, self.get_view(), name=self.name)
        ]


class DBCounterKPI(KPI):

    def get_counts(self, site_identifier=None):
        from .models import KPICounter

        try:
            counter = (KPICounter.objects
                       .get(name=self.name,
                            site_identifier=site_identifier))

        except KPICounter.DoesNotExist:
            logger.warning('KPI {name} was requested for site {site}, but it did not exist'
                           .format(name=self.name,
                                   site=site_identifier))
            return 0, 0

        return counter.value, counter.total

    def increment_counts(self, value_inc, total_inc, site_identifier=None):
        from .models import KPICounter

        affected_rows = (KPICounter.objects
                         .filter(name=self.name, site_identifier=site_identifier)
                         .update(value=F('value') + value_inc,
                                 total=F('total') + total_inc,
                                 last_updated=timezone.now()))

        assert affected_rows <= 1

        if not affected_rows:
            (KPICounter.objects
             .create(name=self.name,
                     site_identifier=site_identifier,
                     value=value_inc, total=total_inc))


class SuccessCounterKPI(DBCounterKPI):

    def register_success(self, site_identifier=None):
        return self.increment_counts(1, 1, site_identifier)

    def register_failure(self, site_identifier=None):
        return self.increment_counts(0, 1, site_identifier)


class MiddlewareKPI(KPI):
    request_types = []
    for_middleware = True

    def should_process(self, url_name, request):

        for kpi_url_name, methods in self.request_types:
            if url_name != kpi_url_name:
                continue

            if methods is not None and request.method not in methods:
                continue

            return True

        return False

    def process(self, request, response):
        raise NotImplementedError


class RequestSuccessKPI(MiddlewareKPI, SuccessCounterKPI):
    count_redirects = False

    def is_failure(self, response):
        return 500 <= response.status_code < 600

    def is_redirect(self, request, response):
        return 300 <= response.status_code < 400

    def get_site_identifier(self, request):
        return None

    def process(self, request, response):

        if not self.count_redirects and self.is_redirect(request, response):
            return

        site_identifier = self.get_site_identifier(request)

        if self.is_failure(response):
            self.register_failure(site_identifier=site_identifier)
        else:
            self.register_success(site_identifier=site_identifier)


class DOMLoadedFastKPI(SuccessCounterKPI):
    receive_path = 'receive/dom_load/'
    max_loading_time_ms = 3000

    def get_site_identifier(self, request):
        return None

    def receive_view(self, request):

        if not request.user.is_authenticated():
            return HttpResponse(status=403)

        if not request.method == 'POST':
            return HttpResponse(status=405)

        try:
            content = json.loads(request.body.decode())
        except json.JSONDecodeError:
            return HttpResponse(status=400)

        try:
            dom_loaded_in_ms = content['dom_loaded_in_ms']
        except KeyError:
            return HttpResponse(status=400)

        site_identifier = self.get_site_identifier(request)

        if dom_loaded_in_ms > self.max_loading_time_ms:
            self.register_failure(site_identifier=site_identifier)
        else:
            self.register_success(site_identifier=site_identifier)

        return HttpResponse(status=200)

    def get_urls(self):
        url_patterns = super().get_urls()

        url_patterns += [
            path(self.receive_path, self.receive_view,
                 name='receive_dom_load_time')
        ]

        return url_patterns

from django.db import models
from django.utils import timezone


class KPICounter(models.Model):

    name = models.CharField(max_length=255)
    site_identifier = models.CharField(max_length=100,  null=True, blank=True)
    value = models.IntegerField(default=0)
    total = models.IntegerField(default=0)
    last_updated = models.DateTimeField(default=timezone.now)

    def __repr__(self):
        return "<KPICounter(name={name}, value={value}, total={total})>".format(
            name=self.name, value=self.value, total=self.total
        )

    __str__ = __repr__

    class Meta:
        unique_together = ('name', 'site_identifier')

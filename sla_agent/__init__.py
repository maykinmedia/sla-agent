default_app_config = 'sla_agent.apps.SlaAgentConfig'

from .agent import sla_agent
from .kpi import KPI, RequestSuccessKPI



.. sla_agent documentation master file, created by startproject.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sla_agent's documentation!
=================================================

:Version: 0.1.0
:Source: https://bitbucket.org/maykinmedia/sla_agent
:Keywords: ``<keywords>``
:PythonVersion: 3.6

|build-status| |requirements| |coverage|

|python-versions| |django-versions| |pypi-version|

<One liner describing the project>

.. contents::

.. section-numbering::

Features
========

* ...
* ...

Installation
============

Requirements
------------

* Python 3.6 or above
* setuptools 30.3.0 or above
* Django 1.11 or above


Install
-------

.. code-block:: bash

    pip install sla_agent


Usage
=====

<document or refer to docs>



.. |build-status| image:: https://travis-ci.org/maykinmedia/sla_agent.svg?branch=develop
    :target: https://travis-ci.org/maykinmedia/sla_agent

.. |requirements| image:: https://requires.io/github/maykinmedia/sla_agent/requirements.svg?branch=develop
    :target: https://requires.io/github/maykinmedia/sla_agent/requirements/?branch=develop
    :alt: Requirements status

.. |coverage| image:: https://codecov.io/gh/maykinmedia/sla_agent/branch/develop/graph/badge.svg
    :target: https://codecov.io/gh/maykinmedia/sla_agent
    :alt: Coverage status

.. |python-versions| image:: https://img.shields.io/pypi/pyversions/sla_agent.svg

.. |django-versions| image:: https://img.shields.io/pypi/djversions/sla_agent.svg

.. |pypi-version| image:: https://img.shields.io/pypi/v/sla_agent.svg
    :target: https://pypi.org/project/sla_agent/

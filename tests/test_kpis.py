import json
from unittest.mock import Mock

from django.test import RequestFactory, TestCase, override_settings

from sla_agent.kpi import (
    KPI, DBCounterKPI, DOMLoadedFastKPI, MiddlewareKPI, RequestSuccessKPI
)
from sla_agent.models import KPICounter

from .factories import KPICounterFactory


class KPITests(TestCase):

    @override_settings(SLA_SITE_TOKEN='letmein')
    def test_simple(self):

        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self, site_identifier=None):
                return 12, 42

        view = SimpleKPI().get_view()

        request = RequestFactory().get('/some/path', {'token': 'letmein'})

        response = view(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json.loads(response.content), {'value': 12, 'total': 42})

    def test_no_auth(self):
        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self, site_identifier=None):
                return 12, 42

        view = SimpleKPI().get_view()

        request = RequestFactory().get('/some/path')

        response = view(request)

        self.assertEqual(response.status_code, 403)

    @override_settings(SLA_SITE_TOKEN='letmein')
    def test_wrong_auth(self):
        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self, site_identifier=None):
                return 12, 42

        view = SimpleKPI().get_view()

        request = RequestFactory().get('/some/path', {'token': 'haxx0r'})

        response = view(request)

        self.assertEqual(response.status_code, 403)

    @override_settings(SLA_SITE_TOKEN='letmein')
    def test_with_site_identifier(self):

        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self, site_identifier=None):
                if not site_identifier:
                    raise Exception

                return 12, 42

        view = SimpleKPI().get_view()

        request = RequestFactory().get('/some/path', {'token': 'letmein', 'site': 'some_site'})

        response = view(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json.loads(response.content), {'value': 12, 'total': 42})


class DBCounterKPITests(TestCase):

    def test_increment(self):

        kpi = DBCounterKPI()
        kpi.name = 'counter'

        kpi.increment_counts(20, 100)

        kpi_counter = KPICounter.objects.get()

        self.assertEqual(kpi_counter.value, 20)
        self.assertEqual(kpi_counter.total, 100)
        self.assertEqual(kpi_counter.site_identifier, None)

        self.assertEqual(kpi.get_counts(), (20, 100))

        kpi.increment_counts(5, 10)

        kpi_counter.refresh_from_db()

        self.assertEqual(kpi_counter.value, 25)
        self.assertEqual(kpi_counter.total, 110)

        self.assertEqual(kpi.get_counts(), (25, 110))

    def test_increment_already_existing_counter(self):
        KPICounterFactory.create(name='counter', site_identifier='some_site',
                                 value=10, total=50)

        KPICounterFactory.create(name='counter', site_identifier=None,
                                 value=10, total=50)

        kpi = DBCounterKPI()
        kpi.name = 'counter'

        kpi.increment_counts(20, 100, site_identifier='some_site')

        kpi_counter = KPICounter.objects.get(site_identifier='some_site')

        self.assertEqual(kpi_counter.value, 30)
        self.assertEqual(kpi_counter.total, 150)
        self.assertEqual(kpi_counter.site_identifier, 'some_site')


    def test_increment_with_site_identifier(self):

        kpi = DBCounterKPI()
        kpi.name = 'counter'

        kpi.increment_counts(20, 100, site_identifier='some_site')

        kpi_counter = KPICounter.objects.get()

        self.assertEqual(kpi_counter.value, 20)
        self.assertEqual(kpi_counter.total, 100)
        self.assertEqual(kpi_counter.site_identifier, 'some_site')

        self.assertEqual(kpi.get_counts('some_site'), (20, 100))
        self.assertEqual(kpi.get_counts(), (0, 0))

        kpi.increment_counts(5, 10, site_identifier='some_site')

        kpi_counter.refresh_from_db()

        self.assertEqual(kpi_counter.value, 25)
        self.assertEqual(kpi_counter.total, 110)

        self.assertEqual(kpi.get_counts(site_identifier='some_site'),
                         (25, 110))

    @override_settings(SLA_SITE_TOKEN='letmein')
    def test_view(self):

        kpi = DBCounterKPI()
        kpi.name = 'counter'
        kpi.initialize()
        kpi.increment_counts(20, 100)

        view = kpi.get_view()
        request = RequestFactory().get('/some/path', {'token': 'letmein'})
        response = view(request)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 20, 'total': 100})

    @override_settings(SLA_SITE_TOKEN='letmein')
    def test_view_with_site_identifier(self):

        kpi = DBCounterKPI()
        kpi.name = 'counter'
        kpi.increment_counts(20, 100, site_identifier='some_site')
        kpi.increment_counts(15, 100, site_identifier='some_other_site')
        kpi.increment_counts(2, 60)

        view = kpi.get_view()
        request = RequestFactory().get('/some/path', {'token': 'letmein', 'site': 'some_site'})
        response = view(request)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 20, 'total': 100})

        view = kpi.get_view()
        request = RequestFactory().get('/some/path',
                                       {'token': 'letmein', 'site': 'some_other_site'})
        response = view(request)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 15, 'total': 100})


class MiddlewareKPITests(KPI):

    def test_should_process(self):

        class SimpleMiddlewareKPI(MiddlewareKPI):
            name = 'middleware'
            request_types = [
                ('ns:some_url_name', ('GET', )),
                ('ns:some_other_url_name', ('DELETE', 'PUT')),
            ]

        kpi = SimpleMiddlewareKPI()

        self.assertTrue(kpi.should_process('ns:some_url_name', Mock(method='GET')))
        self.assertTrue(kpi.should_process('ns:some_other_url_name', Mock(method='DELETE')))
        self.assertTrue(kpi.should_process('ns:some_other_url_name', Mock(method='PUT')))

        self.assertFalse(kpi.should_process('ns:some_url_name', Mock(method='POST')))
        self.assertFalse(kpi.should_process('ns:some_url_name', Mock(method='PUT')))
        self.assertFalse(kpi.should_process('ns:some_other_url_name', Mock(method='GET')))
        self.assertFalse(kpi.should_process('ns:some_other_url_name', Mock(method='POST')))
        self.assertFalse(kpi.should_process('ns:some_unregistered_url', Mock(method='POST')))

    def test_should_process_methods_none(self):

        class SimpleMiddlewareKPI(MiddlewareKPI):
            name = 'middleware'
            request_types = [
                ('ns:some_url_name', None),
            ]

        kpi = SimpleMiddlewareKPI()
        self.assertTrue(kpi.should_process('ns:some_url_name', Mock(method='GET')))
        self.assertTrue(kpi.should_process('ns:some_url_name', Mock(method='POST')))
        self.assertTrue(kpi.should_process('ns:some_url_name', Mock(method='PUT')))


class RequestSuccessKPITests(TestCase):

    def test_simple(self):
        kpi = RequestSuccessKPI()
        kpi.name = 'request_success'
        kpi.initialize()

        kpi.process(Mock(), Mock(status_code=200))

        self.assertEqual(KPICounter.objects.count(), 1)

        self.assertEqual(kpi.get_counts(), (1, 1))

    def test_redirects_dont_count(self):
        kpi = RequestSuccessKPI()
        kpi.name = 'request_success'

        kpi.process(Mock(), Mock(status_code=302))

        self.assertEqual(kpi.get_counts(), (0, 0))


class DOMLoadedFastKPITests(TestCase):

    def test_receive_view_success(self):

        kpi = DOMLoadedFastKPI()
        kpi.name = 'dom_load'
        kpi.max_loading_time = 3000
        kpi.receive_path = 'receive/dom_load/'

        urlpatterns = kpi.get_urls()

        receive_pattern = next(pat for pat in urlpatterns if pat.name == 'receive_dom_load_time')

        receive_view = receive_pattern.callback

        user = Mock()
        user.is_authenticated = Mock(return_value=True)

        request = RequestFactory().post('/sla/receive/dom_load', {'dom_loaded_in_ms': 1000},
                                        content_type='application/json')

        setattr(request, 'user', user)

        response = receive_view(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(kpi.get_counts(), (1, 1))

    def test_receive_view_fail(self):

        kpi = DOMLoadedFastKPI()
        kpi.name = 'dom_load'
        kpi.max_loading_time = 3000
        kpi.receive_path = 'receive/dom_load/'

        urlpatterns = kpi.get_urls()

        receive_pattern = next(pat for pat in urlpatterns if pat.name == 'receive_dom_load_time')

        receive_view = receive_pattern.callback

        user = Mock()
        user.is_authenticated = Mock(return_value=True)

        request = RequestFactory().post('/sla/receive/dom_load', {'dom_loaded_in_ms': 3005},
                                        content_type='application/json')

        setattr(request, 'user', user)

        response = receive_view(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(kpi.get_counts(), (0, 1))

    def test_receive_view_with_site_identifier(self):

        class SiteDOMLoadedFastKPI(DOMLoadedFastKPI):
            name = 'dom_load'
            max_loading_time_ms = 3000
            receive_path = 'receive/dom_load/'

            def get_site_identifier(self, request):
                return request.site

        kpi = SiteDOMLoadedFastKPI()

        user = Mock()
        user.is_authenticated = Mock(return_value=True)

        request = RequestFactory().post('/sla/receive/dom_load', {'dom_loaded_in_ms': 2000},
                                        content_type='application/json')

        setattr(request, 'user', user)
        setattr(request, 'site', 'some_site')

        response = kpi.receive_view(request)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(kpi.get_counts(None), (0, 0))
        self.assertEqual(kpi.get_counts('some_site'), (1, 1))

    def test_receive_view_malformed_json(self):

        kpi = DOMLoadedFastKPI()
        kpi.name = 'dom_load'
        kpi.max_loading_time = 3000
        kpi.receive_path = 'receive/dom_load/'

        user = Mock()
        user.is_authenticated = Mock(return_value=True)

        request = RequestFactory().post('/sla/receive/dom_load', 'HELLOWASSUP',
                                        content_type='application/json')

        setattr(request, 'user', user)

        response = kpi.receive_view(request)

        self.assertEqual(response.status_code, 400)

        self.assertEqual(kpi.get_counts(), (0, 0))

    def test_receive_view_key_not_present(self):

        kpi = DOMLoadedFastKPI()
        kpi.name = 'dom_load'
        kpi.max_loading_time = 3000
        kpi.receive_path = 'receive/dom_load/'

        user = Mock()
        user.is_authenticated = Mock(return_value=True)

        request = RequestFactory().post('/sla/receive/dom_load', {'something': 14},
                                        content_type='application/json')

        setattr(request, 'user', user)

        response = kpi.receive_view(request)

        self.assertEqual(response.status_code, 400)

        self.assertEqual(kpi.get_counts(), (0, 0))

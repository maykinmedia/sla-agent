import json

from django.test import TestCase, override_settings
from django.urls import reverse
from django.http import HttpResponse

from sla_agent.kpi import RequestSuccessKPI
from sla_agent.models import KPICounter

from unittest.mock import patch


@override_settings(SLA_SITE_TOKEN='letmein')
class SLAAgentIntegrationTests(TestCase):
    # this tests the setup in testapp.views

    def test_simple(self):
        KPICounter.objects.create(name='success_kpi2', value=10, total=20)

        kpi1_url = reverse('sla:success_kpi1')
        self.assertEqual(kpi1_url, '/sla/success_kpi1')

        response = self.client.get(kpi1_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 0, 'total': 0})

        self.client.get(reverse('some_url_name', kwargs={'status': 200}))

        response = self.client.get(kpi1_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 1, 'total': 1})

        self.client.get(reverse('some_url_name', kwargs={'status': 500}))
        response = self.client.get(kpi1_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 1, 'total': 2})

        # SomeRequestSucessKPI should only count GET requests, so should ignore this one
        self.client.post(reverse('some_url_name', kwargs={'status': 500}))
        response = self.client.get(kpi1_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 1, 'total': 2})


        kpi2_url = reverse('sla:success_kpi2')
        self.assertEqual(kpi2_url, '/sla/success_kpi2')

        response = self.client.get(kpi2_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 10, 'total': 20})

        self.client.get(reverse('some_other_url_name', kwargs={'status': 200}))
        response = self.client.get(kpi2_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 11, 'total': 21})

        self.client.get(reverse('some_other_url_name', kwargs={'status': 500}))
        response = self.client.get(kpi2_url, {'token': 'letmein'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {'value': 11, 'total': 22})

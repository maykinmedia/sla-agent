import factory


class KPICounterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'sla_agent.KPICounter'

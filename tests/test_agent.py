import warnings
from unittest.mock import Mock

from django.core.exceptions import ImproperlyConfigured
from django.test import TestCase, override_settings

from sla_agent.agent import SLAAgent
from sla_agent.kpi import KPI, MiddlewareKPI


@override_settings(SLA_SITE_TOKEN='letmein')
class SLAAgentTests(TestCase):

    def test_simple(self):
        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self):
                return 1, 2

        agent = SLAAgent()
        agent.register(SimpleKPI)

        kpi = agent.get_kpi('simple')

        self.assertEqual(kpi.__class__, SimpleKPI)

    def test_urls(self):

        class SimpleKPI(KPI):
            name = 'simple'

        agent = SLAAgent()
        agent.register(SimpleKPI)

        patterns = agent.get_urls()
        pattern = patterns[0]

        self.assertEqual(pattern.pattern.name, 'simple')

        self.assertEqual(pattern.callback.__name__, 'simple')

    def test_kpi_doesnt_exist(self):
        class SimpleKPI(KPI):
            name = 'simple'

            def get_counts(self):
                return 1, 2

        agent = SLAAgent()
        agent.register(SimpleKPI)

        with self.assertRaises(ValueError):
            agent.get_kpi('something_else')

    def test_kpi_no_name(self):
        class SimpleKPI(KPI):
            pass

        agent = SLAAgent()
        agent.register(SimpleKPI)

        with self.assertRaises(ImproperlyConfigured):
            agent.check()

    def test_duplicate_names(self):
        class SimpleKPI(KPI):
            name = 'simple'

        class SimpleKPI2(KPI):
            name = 'simple'

        agent = SLAAgent()
        agent.register(SimpleKPI)
        agent.register(SimpleKPI2)

        with self.assertRaises(ImproperlyConfigured):
            agent.check()

    @override_settings(SLA_SITE_TOKEN=None)
    def test_no_site_token_set(self):
        class SimpleKPI(KPI):
            name = 'simple'

        agent = SLAAgent()
        agent.register(SimpleKPI)

        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            agent.check()

            self.assertEqual(len(w), 1)
            self.assertEqual(str(w[-1].message), "SLA_SITE_TOKEN setting not set")


    def test_initialize(self):
        class SimpleKPI(KPI):
            name = 'simple'

            initialize = Mock()

        class SimpleKPI2(KPI):
            name = 'simple2'

            initialize = Mock()

        agent = SLAAgent()
        agent.register(SimpleKPI)
        agent.register(SimpleKPI2)

        agent.get_urls()

        SimpleKPI.initialize.assert_called_once()
        SimpleKPI2.initialize.assert_called_once()

    def test_process_not_for_middleware(self):

        class SimpleKPI(KPI):
            name = 'simple'

            should_process = Mock(return_value=True)
            process = Mock()

        agent = SLAAgent()
        agent.register(SimpleKPI)

        agent.process('url_name', 'GET', Mock())

        SimpleKPI.should_process.assert_not_called()
        SimpleKPI.process.assert_not_called()

    def test_process(self):
        class SimpleKPI(MiddlewareKPI):
            name = 'simple'

            should_process = Mock(return_value=True)
            process = Mock()

        agent = SLAAgent()
        agent.register(SimpleKPI, for_middleware=True)

        agent.process('url_name', 'GET', Mock())

        SimpleKPI.should_process.assert_called_once()
        SimpleKPI.process.assert_called_once()

    def test_process_for_middleware_on_class(self):
        class SimpleKPI(KPI):
            name = 'simple'
            for_middleware = True

            should_process = Mock(return_value=True)
            process = Mock()

        agent = SLAAgent()
        agent.register(SimpleKPI)

        agent.process('url_name', 'GET', Mock())

        SimpleKPI.should_process.assert_called_once()
        SimpleKPI.process.assert_called_once()

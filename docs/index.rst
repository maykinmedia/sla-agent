.. sla_agent documentation master file, created by startproject.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sla_agent's documentation!
=================================================

|build-status| |requirements| |coverage|

|python-versions| |django-versions| |pypi-version|

<One liner describing the project>

Features
========

* ...
* ...

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   quickstart



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. |build-status| image:: https://travis-ci.org/maykinmedia/sla_agent.svg?branch=develop
    :target: https://travis-ci.org/maykinmedia/sla_agent

.. |requirements| image:: https://requires.io/github/maykinmedia/sla_agent/requirements.svg?branch=develop
    :target: https://requires.io/github/maykinmedia/sla_agent/requirements/?branch=develop
    :alt: Requirements status

.. |coverage| image:: https://codecov.io/gh/maykinmedia/sla_agent/branch/develop/graph/badge.svg
    :target: https://codecov.io/gh/maykinmedia/sla_agent
    :alt: Coverage status

.. |python-versions| image:: https://img.shields.io/pypi/pyversions/sla_agent.svg

.. |django-versions| image:: https://img.shields.io/pypi/djversions/sla_agent.svg

.. |pypi-version| image:: https://img.shields.io/pypi/v/sla_agent.svg
    :target: https://pypi.org/project/sla_agent/

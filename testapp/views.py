from django.http import HttpResponse

from django.views import View

from sla_agent.kpi import RequestSuccessKPI
from sla_agent import sla_agent


class SomeView(View):

    def dispatch(self, request, *args, **kwargs):
        super().dispatch(request, *args, **kwargs)

        status = self.kwargs.get('status', 200)

        return HttpResponse(status=status)

    def get(self, *args, **kwargs):
        pass

    def post(self, *args, **kwargs):
        pass


class SomeRequestSuccessKPI(RequestSuccessKPI):
    name = 'success_kpi1'

    request_types = [
        ('some_url_name', ('GET', )),
    ]


sla_agent.register(SomeRequestSuccessKPI)


class AnotherRequestSuccessKPI(RequestSuccessKPI):
    name = 'success_kpi2'

    request_types = [
        ('some_other_url_name', None),
    ]


sla_agent.register(AnotherRequestSuccessKPI)

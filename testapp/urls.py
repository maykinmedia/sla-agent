from django.contrib import admin
from django.urls import path

from testapp.views import SomeView

from sla_agent import sla_agent

urlpatterns = [
    path('sla/', sla_agent.urls),

    path('some_url/<int:status>', SomeView.as_view(), name='some_url_name'),
    path('some_other_url/<int:status>', SomeView.as_view(), name='some_other_url_name')
]
